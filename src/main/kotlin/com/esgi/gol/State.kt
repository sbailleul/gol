package com.esgi.gol

enum class State(val state: String) {
    ALIVE("ALIVE"), DEAD("DEAD")
}