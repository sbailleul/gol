package com.esgi.gol

import com.esgi.gol.CellsManager.processNextStates
import com.esgi.gol.CellsManager.processState
import com.esgi.gol.CellsManager.setNeighbours
import com.esgi.gol.GridRenderer.render
import com.esgi.gol.IOAdapter.convertBoolToCell
import com.esgi.gol.IOAdapter.convertCellsToBool
import com.esgi.gol.rules.CellDieRule
import com.esgi.gol.rules.CellLiveRule
import java.awt.Point

class GameOfLife(grid: Array<BooleanArray>) {
    private var cells: Array<Array<Cell>> = convertBoolToCell(grid, CellLiveRule(), CellDieRule())
    private var generationCnt = 0;

    fun generate(): Array<BooleanArray> {
        processNextStates(cells)
        processState(cells)
        val newStateGrid = convertCellsToBool(cells)
        generationCnt++
        return newStateGrid
    }

    fun generate(generationLimit: Int) {
        for (i in 0..generationLimit) {
            generate()
        }
    }

    fun addCellsToPositions(vararg cellsPositions: Point) {
        cellsPositions.forEach { p -> cells[p.y][p.x] = Cell(State.ALIVE, CellDieRule(), CellLiveRule()) }
        setNeighbours(cells)
    }

    fun getCellByPosition(position: Point): Cell {
        return cells[position.y][position.x]
    }

    init {
        setNeighbours(cells)
    }
}