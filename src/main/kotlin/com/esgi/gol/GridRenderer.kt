package com.esgi.gol

object GridRenderer {
    @JvmStatic
    fun render(cells: Array<BooleanArray>): String {
        return buildString {
            for (cell in cells) {
                for (b in cell) {
                    append(if (b) 1 else 0)
                }
                appendLine()
            }
        }
    }
}