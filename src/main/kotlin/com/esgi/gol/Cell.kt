package com.esgi.gol

import com.esgi.gol.rules.DeadRule
import com.esgi.gol.rules.LifeRule

class Cell(var state: State, private val dieRule: DeadRule, private val liveRule: LifeRule) {
    var nextState = State.DEAD
    val neighbours = mutableSetOf<Cell>()
    fun setNextState() {
        dieRule.die(this)
        liveRule.live(this)
    }

    fun setCurrentState() {
        state = nextState
    }

}