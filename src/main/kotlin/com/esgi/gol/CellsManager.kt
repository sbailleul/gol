package com.esgi.gol

import com.esgi.gol.rules.CellLiveRule
import com.esgi.gol.rules.CellDieRule
import java.util.ArrayList

object CellsManager {
    fun getCellsWithState(cnt: Int, state: State?, liveRull: CellLiveRule?, dieRull: CellDieRule?): ArrayList<Cell> {
        val cells = ArrayList<Cell>(cnt)
        for (i in 0 until cnt) {
            cells.add(Cell(state!!, dieRull!!, liveRull!!))
        }
        return cells
    }

    fun setNeighbours(cells: Array<Array<Cell>>) {
        for (i in cells.indices) {
            for (j in cells[i].indices) {
                val currentCell = cells[i][j]
                if (i > 0) {
                    if (j > 0) {
                        currentCell.neighbours.add(cells[i - 1][j - 1])
                    }
                    if (j < cells[i].size - 1) {
                        currentCell.neighbours.add(cells[i - 1][j + 1])
                    }
                    currentCell.neighbours.add(cells[i - 1][j])
                }
                if (i < cells.size - 1) {
                    if (j > 0) {
                        currentCell.neighbours.add(cells[i + 1][j - 1])
                    }
                    if (j < cells[i].size - 1) {
                        currentCell.neighbours.add(cells[i + 1][j + 1])
                    }
                    currentCell.neighbours.add(cells[i + 1][j])
                }
                if (j > 0) {
                    currentCell.neighbours.add(cells[i][j - 1])
                }
                if (j < cells[i].size - 1) {
                    currentCell.neighbours.add(cells[i][j + 1])
                }
            }
        }
    }

    @JvmStatic
    fun processNextStates(cells: Array<Array<Cell>>) {
        for (cell in cells) {
            for (cell1 in cell) {
                cell1.setNextState()
            }
        }
    }

    @JvmStatic
    fun processState(cells: Array<Array<Cell>>) {
        for (cell in cells) {
            for (cell1 in cell) {
                cell1.setCurrentState()
            }
        }
    }
}