package com.esgi.gol.rules

import com.esgi.gol.Cell
import com.esgi.gol.State

class CellDieRule : DeadRule {
    var maxAliveNeighbours = 3
    var minAliveNeighbours = 2
    override fun die(cell: Cell): Boolean {
        val aliveCnt = cell.neighbours.stream().filter { neighbour -> neighbour.state === State.ALIVE }.count()
        if (cell.state === State.ALIVE && aliveCnt > maxAliveNeighbours) {
            cell.nextState = State.DEAD
        }
        if (cell.state === State.ALIVE && aliveCnt < minAliveNeighbours) {
            cell.nextState = State.DEAD
        }
        return cell.nextState === State.ALIVE
    }
}