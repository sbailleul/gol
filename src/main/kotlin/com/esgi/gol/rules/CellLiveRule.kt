package com.esgi.gol.rules

import com.esgi.gol.Cell
import com.esgi.gol.State

class CellLiveRule : LifeRule {
    var minNeighboursStep = 2
    var maxNeighboursStep = 3
    override fun live(cell: Cell): Boolean {
        val aliveCnt  = cell.neighbours.stream().filter { neighbour -> neighbour.state === State.ALIVE }.count().toInt()
        if (cell.state === State.DEAD && aliveCnt == maxNeighboursStep) {
            cell.nextState = State.ALIVE
        }
        if (cell.state === State.ALIVE && (aliveCnt == maxNeighboursStep || aliveCnt == minNeighboursStep)) {
            cell.nextState = State.ALIVE
        }
        return cell.nextState === State.ALIVE
    }
}