package com.esgi.gol.rules

import com.esgi.gol.Cell

interface LifeRule {
    fun live(cell: Cell): Boolean
}