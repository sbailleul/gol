package com.esgi.gol.rules

import com.esgi.gol.Cell

interface DeadRule {
    fun die(cell: Cell): Boolean
}