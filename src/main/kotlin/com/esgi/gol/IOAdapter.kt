package com.esgi.gol

import com.esgi.gol.rules.CellDieRule
import com.esgi.gol.rules.CellLiveRule
import java.awt.Point

object IOAdapter {

    fun convertBoolToCell(
        grid: Array<BooleanArray>,
        liveRule: CellLiveRule,
        dieRule: CellDieRule
    ): Array<Array<Cell>> {
        return Array(grid.size) { i ->
            Array(grid[0].size) { j ->
                val state = if (grid[i][j]) State.ALIVE else State.DEAD
                Cell(state, dieRule, liveRule)
            }
        }
    }

    fun convertCellsToBool(cells: Array<Array<Cell>>): Array<BooleanArray> {
        val grid = Array(cells.size) { BooleanArray(cells[0].size) }
        for (i in cells.indices) {
            for (j in cells[i].indices) {
                val state = cells[i][j].state == State.ALIVE
                grid[i][j] = state
            }
        }
        return grid
    }

    fun parsePositions(positions: String?): Array<Point> {
        if (positions.isNullOrBlank()) return arrayOf()
        val intPositions = positions
            .split(',')
            .map { p -> p.replace(Regex("\\D+"), "").toInt() }
        val positionsList = mutableListOf<Point>()
        for (i in intPositions.indices) {
            if (i % 2 == 0) {
                positionsList += Point(intPositions[i], intPositions[i + 1])
            }
        }
        return positionsList.toTypedArray()
    }

    fun createMatrix(sizeX: Int, sizeY: Int): Array<BooleanArray> {
        return Array(sizeY) { BooleanArray(sizeX) { false } }
    }

}