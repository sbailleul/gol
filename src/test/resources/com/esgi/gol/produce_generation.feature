Feature: Produce a new game of life generation

  Rule:
  An alive cell which has less than two neighbour alive cells dies by undercrowding
  An alive cell which has more than three neighbour alive cells dies by overcrowding
  An alive cell which has exactly two or three neighbour alive cells stays alive
  A dead cell which has exactly two neighbour alive cells becomes alive

    Scenario: Generate undercrowded population for one alive cell
      Given This population
        |0|0|0|
        |0|0|0|
        |0|0|0|
      When A new generation is produced
      Then Cell in position (1,1) should be "DEAD"
      Given This population
        |0|1|0|
        |0|0|0|
        |0|0|0|
      When A new generation is produced
      Then Cell in position (1,1) should be "DEAD"

    Scenario: Generate overcrowded population for one alive cell
      Given This population
        |1|1|1|
        |1|0|1|
        |0|0|0|
      When A new generation is produced
      Then Cell in position (1,1) should be "DEAD"
      Given This population
        |0|1|1|
        |1|0|1|
        |0|0|0|
      When A new generation is produced
      Then Cell in position (1,1) should be "DEAD"

    Scenario: Generate normal population for one alive cell
      Given This population
        |0|1|1|
        |1|1|0|
        |0|0|0|
      When A new generation is produced
      Then Cell in position (1,1) should be "ALIVE"
      Given This population
        |0|1|1|
        |0|1|0|
        |0|0|0|
      When A new generation is produced
      Then Cell in position (1,1) should be "ALIVE"

    Scenario: Generate normal population for dead cell resurrection purpose
      Given This population
        |0|1|1|
        |1|0|0|
        |0|0|0|
      When A new generation is produced
      Then Cell in position (1,1) should be "ALIVE"

    Scenario: Generate new population recalculate all cells states
      Given This population
        |0|0|0|0|
        |0|1|1|1|
        |1|1|1|0|
        |0|0|0|0|
      When A new generation is produced
      Then Population should evolve like this
        |0|0|1|0|
        |1|0|0|1|
        |1|0|0|1|
        |0|1|0|0|

