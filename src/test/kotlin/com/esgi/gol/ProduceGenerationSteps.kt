package com.esgi.gol

import io.cucumber.datatable.DataTable
import io.cucumber.java8.En
import io.cucumber.java8.PendingException
import io.cucumber.java8.StepDefinitionBody
import org.junit.Assert.assertArrayEquals
import org.junit.Assert.assertEquals
import java.awt.Point


class ProduceGenerationSteps : En {
    private lateinit var gameOfLife: GameOfLife;
    private lateinit var generatedPopulation: Array<BooleanArray>

    init {

        When("A new generation is produced") {
            generatedPopulation = this.gameOfLife.generate()
        }

        Given("This population") { dataTable: DataTable ->
            this.gameOfLife = GameOfLife(dataTable.toGameOfLifeMatrix())
        }
        Then("Population should evolve like this") { dataTable: DataTable ->
            val exceptedMatrix = dataTable.toGameOfLifeMatrix()
            generatedPopulation.forEachIndexed { i, column ->
                assertArrayEquals(exceptedMatrix[i], column)
            }
        }
        Then(
            "Cell in position \\({int},{int}) should be {string}"
        ) { cellX: Int, cellY: Int, expectedCellState: String ->
            val testCell = this.gameOfLife.getCellByPosition(Point(cellX, cellY))
            assertEquals(expectedCellState, testCell.state.name)
        }

    }
}