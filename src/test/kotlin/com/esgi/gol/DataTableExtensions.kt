package com.esgi.gol

import io.cucumber.datatable.DataTable

fun DataTable.toGameOfLifeMatrix():Array<BooleanArray> {
    return asLists().map { column ->
        column.map { cell ->
            cell != "0"
        }.toBooleanArray()
    }.toTypedArray()
}