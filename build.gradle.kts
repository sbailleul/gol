plugins {
    kotlin("jvm") version "1.4.21"
}

group = "com.esgi"
version = "1.0-SNAPSHOT"



configurations {}


val cucumberRuntime by configurations.creating {
    extendsFrom(configurations["testImplementation"])
}


repositories {
    mavenCentral()
}


dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    testImplementation("io.cucumber:cucumber-java8:6.9.1")
    testImplementation("io.cucumber:cucumber-junit:6.9.1")
}


tasks.withType<Test> {
    useJUnitPlatform()
}


tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"
    }
}


task("cucumber") {
    dependsOn("assemble", "compileTestJava")
    doLast {
        javaexec {
            main = "io.cucumber.core.cli.Main"
            classpath = cucumberRuntime + sourceSets.main.get().output + sourceSets.test.get().output
            args = listOf("--plugin", "pretty", "--glue", "com.esgi.gol", "src/test/resources")
        }
    }
}